// Modify the echo program to print the index and value of each of its arguments,
// one per line.
package main

import (
	"fmt"
	"os"
)

func main() {
	for i, args := range os.Args[0:] {
		fmt.Printf("Index: %v\tArgument: %s\n", i, args)
	}
}
