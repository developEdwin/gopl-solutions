// Modify the echo program to print os.Args[0], the name of
// the running script
package main

import (
	"fmt"
	"os"
	"strings"
)

func main() {
	fmt.Println(strings.Join(os.Args[0:], " "))
}
