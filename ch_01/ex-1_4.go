// Modify dup2 to print the names of all files in which each duplicated line occurs.
// By clearing the "counts" map with each iteration, one assures that for every file
// the output will be that of the duplicated lines in each file, as well as the number
// of duplicates.
package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	counts := make(map[string]int)
	files := os.Args[1:]
	if len(files) == 0 {
		countLines(os.Stdin, counts)
	} else {
		for _, arg := range files {
			f, err := os.Open(arg)
			if err != nil {
				fmt.Fprintf(os.Stderr, "dup2: %v\n", err)
				continue
			}
			countLines(f, counts)
			for line, n := range counts {
				if n > 1 {
					fmt.Printf("File: %s\t%d\t%s\n", string(arg), n, line)
				}
			}
			counts = make(map[string]int) // Clear the current map
			f.Close()
		}
	}
}

func countLines(f *os.File, counts map[string]int) {
	input := bufio.NewScanner(f)
	for input.Scan() {
		counts[input.Text()]++
	}
	// NOTE: ignoring potential errors from input.Err()
}
