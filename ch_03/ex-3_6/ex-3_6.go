// Supersampling is a technique to reduce the effect of pixelation by computing the color value at several points within each pixel and taking the average.
// The simplest method is to divide each pixel into four ‘‘subpixels.’’ Implement it.
package main

import (
	"image"
	"image/color"
	"image/png"
	"math/cmplx"
	"os"
)

func main() {
	const (
		xmin, ymin, xmax, ymax = -2, -2, +2, +2
		width, height          = 1024, 1024
	)
	offsetX := []float64{-0.005, 0.005}
	offsetY := []float64{-0.005, 0.005}

	img := image.NewRGBA(image.Rect(0, 0, width, height))
	for py := 0; py < height; py++ {
		y := float64(py)/height*(ymax-ymin) + ymin
		for px := 0; px < width; px++ {
			x := float64(px)/width*(xmax-xmin) + xmin
			var pixelArray []color.Color
			for i := 0; i < 2; i++ {
				for j := 0; j < 2; j++ {
					x += offsetX[i]
					y += offsetY[j]
					pixelArray = append(pixelArray, mandelbrot(complex(x, y)))
				}
			}
			img.Set(px, py, colorAvg(pixelArray))
		}
	}
	png.Encode(os.Stdout, img) // Ignoring errors
}

func colorAvg(cArr []color.Color) color.Color {
	var rNew, gNew, bNew, aNew uint16

	nArr := uint32(len(cArr))

	for _, c := range cArr {
		r, g, b, a := c.RGBA()
		rNew += uint16(r / nArr)
		gNew += uint16(g / nArr)
		bNew += uint16(b / nArr)
		aNew += uint16(a / nArr)
	}
	return color.RGBA64{rNew, gNew, bNew, aNew}
}

func mandelbrot(z complex128) color.Color {
	const iterations = 200
	const contrast = 15

	var v complex128
	for n := uint8(0); n < iterations; n++ {
		v = v*v + z
		if cmplx.Abs(v) > 2 {
			return color.Gray{255 - contrast*n}
		}
	}
	return color.Black
}
