// Surface computes an SVG rendering of a 3D surface function
package main

import (
	"fmt"
	"io"
	"log"
	"math"
	"net/http"
	"strconv"
)

const cells = 100 // number of grid cells

var (
	angle   = math.Pi / 6 // angle of x, y axes (30 deg)
	sin30   = math.Sin(angle)
	cos30   = math.Cos(angle)
	width   = 600.0
	height  = 320.0
	xyrange = 30.0                // axis ranges
	xyscale = width / 2 / xyrange // pixels per x or y unit
	zscale  = height * 0.4        // pixels per z unit
)

// define a type for a two-variable function
type zFunc func(x, y float64) float64

func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe("localhost:8000", nil))
}

func handler(w http.ResponseWriter, r *http.Request) {
	// Server must set Content-Type
	w.Header().Set("Content-Type", "image/svg+xml")

	if err := r.ParseForm(); err != nil {
		log.Print(err)
	}

	// Change the width and height according to the user's input
	if widthStr := r.FormValue("width"); widthStr != "" {
		if value, err := strconv.Atoi(widthStr); err == nil {
			width = float64(value)
		}
	}
	if heightStr := r.FormValue("height"); heightStr != "" {
		if value, err := strconv.Atoi(heightStr); err == nil {
			height = float64(value)
		}
	}
	// Create the SVG graphic with just the saddle
	svg(w, f, width, height)
}

// linearGradient returns a gradient of color in hexadecimal between red and blue
// given a single value, using a linear interpolation formula
func linearGradient(x float64, widthVal float64) string {
	d := x / widthVal

	// Define the colors: red and blue
	colorRed := [3]float64{255, 0, 0}
	colorBlue := [3]float64{0, 0, 255}

	// Use linear interpolation
	r := colorRed[0] + d*(colorBlue[0]-colorRed[0])
	g := colorRed[1] + d*(colorBlue[1]-colorRed[1])
	b := colorRed[2] + d*(colorBlue[2]-colorRed[2])

	// Convert to a single value for the whole color
	colorStr := fmt.Sprintf("#%02x%02x%02x", uint8(r), uint8(g), uint8(b))

	return colorStr
}

func svg(out io.Writer, f zFunc, width float64, height float64) {
	// Create a SVG for any type of two-variable function
	fmt.Fprintf(out, "<svg xmlns='http://www.w3.org/2000/svg' "+
		"style='stroke: grey; fill: white; stroke-width: 0.7' "+
		"width='%d' height='%d'> ", int(width), int(height))

	// Create array for storing the corner information
	var cornerxy [cells]float64

	for i := 0; i < cells; i++ {
		for j := 0; j < cells; j++ {
			cornerxy[0], cornerxy[1] = corner(i+1, j, f)
			cornerxy[2], cornerxy[3] = corner(i, j, f)
			cornerxy[4], cornerxy[5] = corner(i, j+1, f)
			cornerxy[6], cornerxy[7] = corner(i+1, j+1, f)
			// Check for non-finite values
			if math.IsNaN(cornerxy[j]) || math.IsInf(cornerxy[j], 0) {
				continue
			}
			fmt.Fprintf(out, "<polygon style='stroke: %s; fill: #222222' points='%g,%g,%g,%g,%g,%g,%g,%g'/>\n",
				linearGradient(f(float64(i), float64(j)), width),
				cornerxy[0], cornerxy[1], cornerxy[2], cornerxy[3],
				cornerxy[4], cornerxy[5], cornerxy[6], cornerxy[7])
		}
	}
	fmt.Fprintln(out, "</svg>")
}

func corner(i, j int, f zFunc) (float64, float64) {
	// Find point (x,y) at corner of cell (i, j)
	x := xyrange * (float64(i)/cells - 0.5)
	y := xyrange * (float64(j)/cells - 0.5)

	// Compute surface height z
	z := f(x, y)

	// Project (x,y,z) isometrically onto 2D SVG canvas (sx,sy)
	sx := width/2 + (x-y)*cos30*xyscale
	sy := height/2 + (x+y)*sin30*xyscale - z*zscale

	return sx, sy
}

func f(x, y float64) float64 {
	r := math.Hypot(x, y) // distance from (0,0)
	return math.Sin(r) / r
}
