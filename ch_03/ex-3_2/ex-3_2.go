// Surface computes an SVG rendering of a 3D surface function
package main

import (
	"flag"
	"fmt"
	"math"
)

const (
	width, height = 600, 320            // canvas size in pixels
	cells         = 100                 // number of grid cells
	xyrange       = 30.0                // axis ranges
	xyscale       = width / 2 / xyrange // pixels per x or y unit
	zscale        = height * 0.4        // pixels per z unit
	angle         = math.Pi / 6         // angle of x, y axes (30 deg)
)

var sin30, cos30 = math.Sin(angle), math.Cos(angle)

// define a type for a two-variable function
type zFunc func(x, y float64) float64

// Define some flags to use
var functionFlag = flag.String("f", " ", "Choose from either saddle or hyperboloid")

func main() {
	// Deal with the flags first
	flag.Parse()

	switch *functionFlag {
	case "saddle":
		svg(saddle)
	case "hyperboloid":
		svg(hyperboloid)
	default:
		fmt.Println("No available function with that name.")
	}
}

func svg(f zFunc) {
	// Create a SVG for any type of two-variable function
	fmt.Printf("<svg xmlns='http://www.w3.org/2000/svg' "+
		"style='stroke: grey; fill: white; stroke-width: 0.7' "+
		"width='%d' height='%d'> ", width, height)

	// Create array for storing the corner information
	var cornerxy [cells]float64

	for i := 0; i < cells; i++ {
		for j := 0; j < cells; j++ {
			cornerxy[0], cornerxy[1] = corner(i+1, j, f)
			cornerxy[2], cornerxy[3] = corner(i, j, f)
			cornerxy[4], cornerxy[5] = corner(i, j+1, f)
			cornerxy[6], cornerxy[7] = corner(i+1, j+1, f)
			// Check for non-finite values
			if math.IsNaN(cornerxy[j]) || math.IsInf(cornerxy[j], 0) {
				continue
			}
			fmt.Printf("<polygon points='%g,%g,%g,%g,%g,%g,%g,%g,/>\n'",
				cornerxy[0], cornerxy[1], cornerxy[2], cornerxy[3], cornerxy[4], cornerxy[5],
				cornerxy[6], cornerxy[7])
		}
	}
	fmt.Printf("</svg>")
}

func corner(i, j int, f zFunc) (float64, float64) {
	// Find point (x,y) at corner of cell (i, j)
	x := xyrange * (float64(i)/cells - 0.5)
	y := xyrange * (float64(j)/cells - 0.5)

	// Compute surface height z
	z := f(x, y)

	// Project (x,y,z) isometrically onto 2D SVG canvas (sx,sy)
	sx := width/2 + (x-y)*cos30*xyscale
	sy := height/2 + (x+y)*sin30*xyscale - z*zscale

	return sx, sy
}

func saddle(x, y float64) float64 {
	return (math.Pow(x, 2.0) - math.Pow(y, 2.0))
}

func hyperboloid(x, y float64) float64 {
	// parameters for the hyperboloid
	a := 1.5
	b := 3.5
	c := 2.0

	z := 1.0 - math.Pow(x/a, 2.0) - math.Pow(y/b, 2.0)

	return (-1.0 * math.Pow(c, 2.0) * z)
}
