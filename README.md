# Solutions for the Go Programming Language book

Solutions for the exercises from the Go Programming Language [book](http://www.gopl.io/).

**_IMPORTANT NOTE_**: Remember to clone this repository under your own $GOPATH/src directory, as it
is an important part of the Go toolchain to do so.

This is done whenever you set up your own workspace directory to work with your own projects.
For more information on this topic be sure to read [this](https://golang.org/doc/install#testing).