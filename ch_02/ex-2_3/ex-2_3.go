// Rewrite PopCount to use a loop instead of a single expression.
// Compare the performance of the two versions.
package main

import (
	"fmt"
	"gopl-solutions/ch_02/ex-2_3/popcount"
)

func main() {
	// Initialize a new variable of type uint8
	var xBytes uint64 = 15

	// Print the population total
	fmt.Printf("Population count of %d: %d\n", xBytes, popcount.PopCount(xBytes))
}
