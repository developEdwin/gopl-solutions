//Add types, constants, and functions to tempconv for processing temperatures in
// the Kelvin scale, where zero Kelvin is −273.15°C and a difference of 1K has the same magni-
// tude as 1°C.
package main

import "fmt"
import "gopl-solutions/ch_02/ex-2_1/tempconv"

func main() {
	var roomTemp tempconv.Celsius = 25
	fmt.Printf("Room temperature in Kelvin: %v\n",
		tempconv.CToK(roomTemp).String())
}
