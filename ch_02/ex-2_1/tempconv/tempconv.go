//Package tempconv performs Celsius, Kelvin and Fahrenheit conversions.
package tempconv

import "fmt"

//Celsius type for the Celsius scale is float64
type Celsius float64

//Fahrenheit type for the Fahrenheit scale is float64
type Fahrenheit float64

//Kelvin type for the Kelvin scale is float64
type Kelvin float64

//Constant temperatures for Kelvin and Celsius scales.
const (
	AbsoluteZeroC Celsius = -273.15
	AbsoluteZeroK Kelvin  = 0
	FreezingC     Celsius = 0
	BiolingC      Celsius = 100
)

//Simple type methods for better string formatting
func (c Celsius) String() string {
	return fmt.Sprintf("%g°C", c)
}

func (f Fahrenheit) String() string {
	return fmt.Sprintf("%g°F", f)
}

func (k Kelvin) String() string {
	return fmt.Sprintf("%g K", k)
}

//CToF converts a Celsius temperature to Fahrenheit
func CToF(c Celsius) Fahrenheit {
	return Fahrenheit(c*9/5 + 32)
}

//FToC converts a Fahrenheit temperature to Celsius
func FToC(f Fahrenheit) Celsius {
	return Celsius((f - 32) * 5 / 9)
}

//KToC converts a Kelvin temperature to Celsius
func KToC(k Kelvin) Celsius {
	return Celsius(k - 273.15)
}

//CToK converts a Celsius temperature to Kelvin
func CToK(c Celsius) Kelvin {
	return Kelvin(c + 273.15)
}

//FToK converts a Fahrenheit temperature to Kelvin
func FToK(f Fahrenheit) Kelvin {
	fTemp := FToC(f)
	return Kelvin(CToK(fTemp))
}

//KToF converts a Kelvin temperature to Fahrenheit
func KToF(k Kelvin) Fahrenheit {
	kelTemp := KToC(k)
	return Fahrenheit(CToF(kelTemp))
}
