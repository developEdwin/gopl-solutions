// Write a version of PopCount that counts bits by shifting its argument through 64 bit positions, testing the rightmost bit each time.
// Compare its performance to the table- lookup version.
package popcount

import "testing"

// pc[i] is the population count of i
var pc [256]byte

func PopCountTableLoop(x uint64) int {
	// Dummy variable
	var sumPc int

	// Iterate and sum
	for i := 0; i < 8; i++ {
		sumPc += int(pc[byte(x>>(uint64(i)*8))])
	}

	return sumPc
}

func PopCountShift(x uint64) int {
	// Dummy variable to count
	var count int

	for i := 0; i < 64; i++ {
		if x > 0 {
			if x&1 == 1 {
				count++
			}
			// Shift the bits
			x >>= 1
		}
	}
	return count
}

func init() {
	for i := range pc {
		pc[i] = pc[i/2] + byte(i&1)
	}
}

// PopCountTable returns the population count (number of set bits) of x.
func PopCountTable(x uint64) int {
	return int(pc[byte(x>>(0*8))] +
		pc[byte(x>>(1*8))] +
		pc[byte(x>>(2*8))] +
		pc[byte(x>>(3*8))] +
		pc[byte(x>>(4*8))] +
		pc[byte(x>>(5*8))] +
		pc[byte(x>>(6*8))] +
		pc[byte(x>>(7*8))])
}

func bench(b *testing.B, f func(uint64) int) {
	for i := 0; i < b.N; i++ {
		f(uint64(i))
	}
}

func BenchmarkTable(b *testing.B) {
	bench(b, PopCountTable)
}

func BenchmarkTableLoop(b *testing.B) {
	bench(b, PopCountTableLoop)
}

func BenchmarkShift(b *testing.B) {
	bench(b, PopCountShift)
}
