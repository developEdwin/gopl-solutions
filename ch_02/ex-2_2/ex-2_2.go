// Write a general-purpose unit-conversion program analogous to cf that reads
// numbers from its command-line arguments or from the standard input if there are no argu-
// ments, and converts each number into units like temperature in Celsius and Fahrenheit,
// length in feet and meters, weight in pounds and kilograms, and the like.
package main

import (
	"bufio"
	"fmt"
	"gopl-solutions/ch_02/ex-2_1/tempconv"
	"gopl-solutions/ch_02/ex-2_2/conversion"
	"os"
	"strconv"
)

func main() {
	if len(os.Args) > 1 {
		for _, arg := range os.Args[1:] {
			value, err := strconv.ParseFloat(arg, 64)
			if err != nil {
				fmt.Fprintf(os.Stderr, "conversion: %v\n", err)
				os.Exit(1)
			}
			tempConversion(value, err)
			massConversion(value, err)
			lengthConversion(value, err)
		}
	} else {
		input := bufio.NewScanner(os.Stdin)
		for input.Scan() {
			line := input.Text()
			tempConversion(strconv.ParseFloat(line, 64))
			massConversion(strconv.ParseFloat(line, 64))
			lengthConversion(strconv.ParseFloat(line, 64))
		}
	}
}

func tempConversion(t float64, e error) {
	if e != nil {
		fmt.Fprintf(os.Stderr, "temperature conversion: %v\n", e)
		os.Exit(1)
	}
	f := tempconv.Fahrenheit(t)
	c := tempconv.Celsius(t)
	fmt.Printf("%s = %s, %s = %s\n",
		f, tempconv.FToC(f).String(), c, tempconv.CToF(c).String())
}

func massConversion(value float64, e error) {
	if e != nil {
		fmt.Fprintf(os.Stderr, "mass conversion: %v\n", e)
		os.Exit(1)
	}
	p := conversion.Pound(value)
	k := conversion.Kg(value)
	fmt.Printf("%s = %s, %s = %s\n",
		p, conversion.PoundToKg(p).String(), k, conversion.KgToPound(k).String())
}

func lengthConversion(value float64, e error) {
	if e != nil {
		fmt.Fprintf(os.Stderr, "length conversion: %v\n", e)
		os.Exit(1)
	}
	f := conversion.Feet(value)
	m := conversion.Meter(value)
	fmt.Printf("%s = %s, %s = %s\n",
		f, conversion.FeetToMeter(f).String(), m, conversion.MetertoFeet(m).String())
}
