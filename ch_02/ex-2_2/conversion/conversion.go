//Package conversion is a simple package for converting
//some units, namely mass and length.
package conversion

import "fmt"

//Length types

//Feet type for the feet unit is float64
type Feet float64

//Meter type fot the meter unit is float64
type Meter float64

//Mass types

//Pound type for the pound unit is float64
type Pound float64

//Kg type for the kilogram unit is float64
type Kg float64

//Basic numeric constants
const (
	FToM   Feet  = 0.3048
	MToF   Meter = 3.28084
	KgToLb Kg    = 0.4535924
	LbToKg Pound = 2.204623
)

//Simple type methods for better string formatting
func (f Feet) String() string {
	return fmt.Sprintf("%g ft", f)
}

func (m Meter) String() string {
	return fmt.Sprintf("%g m", m)
}

func (p Pound) String() string {
	return fmt.Sprintf("%g lb", p)
}

func (k Kg) String() string {
	return fmt.Sprintf("%g kg", k)
}

//FeetToMeter converts feet to meter units
func FeetToMeter(f Feet) Meter {
	return Meter(f * FToM)
}

//MetertoFeet converts meter to feet units
func MetertoFeet(m Meter) Feet {
	return Feet(m * MToF)
}

//KgToPound converts kilograms to pound units
func KgToPound(k Kg) Pound {
	return Pound(k * KgToLb)
}

//PoundToKg converts kilograms to pound units
func PoundToKg(p Pound) Kg {
	return Kg(p * LbToKg)
}
